##############################################################################
#
# Project: PhenoFly UAS data processing tools
# File: Sample script to project iamges on DTM followed by plot-based sample extraction
#
# Author: Lukas Roth (lukas.roth@usys.ethz.ch)
#
# Copyright (C) 2018  ETH Zürich, Lukas Roth (lukas.roth@usys.ethz.ch)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Based on:
# Roth, Aasen, Walter, Liebisch 2018: Extracting leaf area index using viewing
# geometry effects—A new perspective on high-resolution unmanned aerial system
# photography, ISPRS Journal of Photogrammetry and Remote Sensing.
# https://doi.org/10.1016/j.isprsjprs.2018.04.012
#
##############################################################################

# Input:
# - Agisoft camera position file (_TestData/sonyDX100II/camera_position_RGB.txt)
# - Digital terrain model in STL file format (_TestData/sonyDX100II/DTM.stl)
# - Folder with images (_TestData/sonyDX100II/raw/)
# - Polygon sampling layer in geoJSON format (_TestData/sonyDX100II/samples_plots.geojson)
#
# Output:
# - Per-image coordinate and viewpoint information (_Demo_output/dg/)
# - Image boundaries in wold coordinates as geoJSON (_Demo_output/dg/DG_field_of_views.geojson)
# - Extracted plot-based image parts (_Demo_output/samples/)
# - Viewpoint of each extracted sample (_Demo_output/samples/viewpoint.csv)
#

from ImageProjectionSampling import ReformatAgisoftCameraPositionFile
from ImageProjectionSampling import DirectGeoreference
from ImageProjectionSampling import ExtractSamples

import os
import shutil

input_directory = '_TestData/sonyDX100II'
output_directory = '_Demo_output'

# Prepare output directory, delete content
shutil.rmtree(output_directory, ignore_errors=True)
os.mkdir(output_directory)

# Set path to files and folder
camera_position_agisoft_file = os.path.join(input_directory, 'camera_position_RGB.txt')
camera_position_csv_file = os.path.join(output_directory, 'camera_position_RGB.csv')
DTM_STL_file = os.path.join(input_directory, 'DTM.stl')
sampling_geojson_file = os.path.join(input_directory, 'samples_plots.geojson')
origin_images_folder = os.path.join(input_directory, 'raw')
direct_georeference_output_folder = os.path.join(output_directory, 'dg')
sampling_output_folder = os.path.join(output_directory, 'samples')

# Format camera position file
ReformatAgisoftCameraPositionFile.process(camera_position_agisoft_file,
                                          camera_position_csv_file)

# Project images to DTM
os.mkdir(direct_georeference_output_folder)
DirectGeoreference.process(camera_position_file=camera_position_csv_file,
                               DTM_stl_file=DTM_STL_file,
                               no_of_interpolation_points_x=30, no_of_interpolation_points_y=30,
                               output_directory=direct_georeference_output_folder,
                               epsg=2056, sensor_dimension_x=13.2 / 1000, sensor_dimension_y=8.8 / 1000,
                               focal_length=10.4 / 1000,
                               sensor_resolution_x=5472, sensor_resolution_y=3648,
                               flight_height=28, verbose=False, cx=0, cy=0, k1=0)

# Extract image samples
os.mkdir(sampling_output_folder)
ExtractSamples.process(path_raw_files = origin_images_folder,
                       path_meta_files = direct_georeference_output_folder,
                       sample_geojson = sampling_geojson_file,
                       output_directory = sampling_output_folder,
                       verbose=True)
