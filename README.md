# PhenoFly data processing tools

&copy; Lukas Roth, Group of crop science, ETH Zurich

License: GPL-2 

## Background

Based on:

Roth, Aasen, Walter, Liebisch 2018: Extracting leaf area index using viewing geometry effects—A new perspective on high-resolution unmanned aerial system photography, ISPRS Journal of Photogrammetry and Remote Sensing.
https://doi.org/10.1016/j.isprsjprs.2018.04.012

## Pre-release of newer version:

https://gitlab.ethz.ch/crop_phenotyping/PhenoFly_data_processing_tools/tree/pre_release