##############################################################################
#
# Project: PhenoFly UAS data processing tools
# File: Common functions
#
# Author: Lukas Roth (lukas.roth@usys.ethz.ch)
#
# Copyright (C) 2018  ETH Zürich, Lukas Roth (lukas.roth@usys.ethz.ch)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

# Dependency
import math
import numpy as np
import stl
from osgeo import gdal
from shapely.geometry import Point, Polygon
import gdal


def ray_plane_intersect(ray_point, ray_vector, plane_point, plane_normal, epsilon=1e-64):
    """Intersect ray with plane

    :param ray_point: Numpy array with coordinates of point on ray
    :param ray_vector: Numpy array with direction of ray
    :param plane_point: Numpy array with coordinates of point on plane
    :param plane_normal: Numpy array with normal of plane
    :param epsilon: Epsilon value as float. Intersection distances with a lower value than epsilon are ignored
    :return:
    """

    ndotu = plane_normal.dot(ray_vector)

    if abs(ndotu) < epsilon:
        # no intersection or line is within plane
        return None
    else:
        # Trace ray. Based on:
        # Hughes, J. F. (2014). Computer graphics : principles and practice. Upper Saddle River, N.J: Addison-Wesley, 3rd ed. edition.
        w = ray_point - plane_point
        si = -plane_normal.dot(w) / ndotu
        psi = w + si * ray_vector + plane_point
        return psi


def point_in_polygon(point, polygon):
    """Test if point is inside poylgon. Using

    :param point: Tuple with coordinates of point to test
    :param polygon: List of tuples with coordinates of polygon to test
    :return:
    """
    point = Point(point[0], point[1])
    polygon = Polygon(polygon)
    return polygon.contains(point)


def find_bounding_box_mesh(mesh):
    """Find STL mesh bounding box in xyz

    :param mesh: STL mesh
    :return: minx, maxx, miny, maxy, minz, maxz
    """

    minx = maxx = miny = maxy = minz = maxz = None
    for p in mesh.points:
        maxx = max(value for value in [p[stl.Dimension.X], maxx] if value is not None)
        minx = min(value for value in [p[stl.Dimension.X], minx] if value is not None)
        maxy = max(value for value in [p[stl.Dimension.Y], maxy] if value is not None)
        miny = min(value for value in [p[stl.Dimension.Y], miny] if value is not None)
        maxz = max(value for value in [p[stl.Dimension.Z], maxz] if value is not None)
        minz = min(value for value in [p[stl.Dimension.Z], minz] if value is not None)

    return minx, maxx, miny, maxy, minz, maxz


def get_triangle_normale(points):
    """Calculates normale of a plane defined by three edge points

    :param points: List of 3 points
    :return: Normale
    """

    a = np.array(points[0])
    b = np.array(points[1])
    c = np.array(points[2])

    v1 = a - b
    v2 = a - c
    normale = np.cross(v1, v2)

    return normale


def rotation_matrix(axis, theta):
    """ Create 3-D rotation matrix
    Based on Euler–Rodrigues formula (https://en.wikipedia.org/wiki/Euler%E2%80%93Rodrigues_formula)

    :param axis: Axis to rotate on
    :param theta: Counterclockwise rotation in radians
    :return: Rotation matrix
    """
    axis = np.asarray(axis)
    axis = axis / math.sqrt(np.dot(axis, axis))
    a = math.cos(theta / 2.0)
    b, c, d = -axis * math.sin(theta / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return np.array([[aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
                     [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
                     [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc]])


def create_nband_GeoTiff(file_path, arrays, data_type, keys=None):
    """Writes geoTiff to filesystem using as many bands as arrays are provided. Optionally annotates metadata per band

    :param file_path: Path to save file
    :param arrays: List ob band arrays
    :param data_type: gdal datatype for all bands
    :param keys: List of dictionaries with metadata per band
    :return:
    """
    driver = gdal.GetDriverByName('GTiff')
    file = file_path

    ds = driver.Create(file, arrays[0].shape[1], arrays[0].shape[0], len(arrays), data_type)

    for i, array in enumerate(arrays):
        band_ = ds.GetRasterBand(i + 1)
        band_.WriteArray(array)
        if keys is not None:
            for key, value in keys[i].items():
                band_.SetMetadataItem(key, str(value))

    ds = None

    return file_path


if __name__ == "__main__":
    print("Nothing to run")
